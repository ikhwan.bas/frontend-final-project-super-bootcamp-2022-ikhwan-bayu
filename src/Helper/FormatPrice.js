const Formatter = (value) => {
    const result = new Intl.NumberFormat('en-ID', {
        style: 'currency',
        currency: 'IDR'
    }).format(value)
        .replace(/[IDR]/gi, '')
        .replace(/(\.+\d{2})/, '')

    return `Rp. ${result}`
}


export default Formatter