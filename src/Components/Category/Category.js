import './category.css';

import motorcycle from "../../image/motorcycle.jpg"
import car from "../../image/car.jpg"
import fashion from "../../image/fashion.jpg"
import house from "../../image/house.jpg"
import service from "../../image/service.jpg"
import laptop from "../../image/laptop.jpg"
import hobby from "../../image/hobby.jpg"
import sport from "../../image/sport.jpg"
import camera from "../../image/camera.jpg"
import handphone from "../../image/handphone.jpg"
import electro from "../../image/electro.jpg"
import kid from "../../image/kid.jpg"



import { Card } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import FormatTitle from '../../Helper/FormatTitle';

const Category = () => {
    const imageCategory = [motorcycle, fashion, car, house, service, hobby, sport, laptop, handphone, camera, electro, kid]
    const [data, setData] = useState([])
    useEffect(() => {
        const fetchCategory = async () => {
            const category = await axios.get("https://new-api-ecommerce.herokuapp.com/product-category")
            setData(category.data.data)
        }
        fetchCategory()
    }, [])

    let navigate = useNavigate()

    return (
        <>
            <div className='card-category-container' >
                {data.map((cat, ind) => {
                    const FormatTitleCategory = FormatTitle(cat.category_name)

                    return (<>
                        <div className='d-block w-15 content-card-category'>
                            <Card onClick={() => { navigate(`/category/${cat.category_name}`) }} style={{ width: '10rem' }}>
                                <div className="shadow-box">
                                    <Card.Img variant="top" src={imageCategory[ind]} />
                                    <Card.Body>
                                        <Card.Title>{FormatTitleCategory}</Card.Title>
                                    </Card.Body>
                                </div>
                            </Card>
                        </div>
                    </>)
                })}

            </div>
        </>
    );
}

export default Category;
