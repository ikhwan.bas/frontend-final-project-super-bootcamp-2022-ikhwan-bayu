import "./footer.css"

import footerimage from "../../image/profile-footer.jpg"


import { Col, Row } from "react-bootstrap"

const FooterComp = () => {

    return (
        <div className="footer-container">
            <div className="footer-image-container">
                <img src={footerimage} alt="storefooter" />
                <img src={footerimage} alt="storefooter" />
            </div>
            <div className="footer-center">
                <Row>
                    <Col sm="12">
                        <footer >Copyright © KulakPedia 2022</footer>
                    </Col>
                </Row>
            </div>
        </div>
    )
}

export default FooterComp