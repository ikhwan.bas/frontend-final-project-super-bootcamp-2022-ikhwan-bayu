import { useContext } from "react"
import { Button } from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import { StoreContext } from "../../Context/storeContext"
import "./sidepanel.css"


const SidePanel = () => {
    const [, setStore] = useContext(StoreContext)

    let navigate = useNavigate()
    const navigateButton = () => {
        navigate('/')
    }
    return (
        <div className="sidepanel">
            <h1>Dashboard</h1>
            <hr />
            <div className="ul-li-comp">
                <h4>Produk</h4>
                <hr />
                <ul>

                    <li>
                        <div className="button-border d-grid gap-2">
                            <Button name="dashboard" onClick={() => { navigate('/seller/dashboard') }} variant="light" size="lg">Semua Produk</Button>
                        </div>
                    </li>
                    <li>
                        <div className="button-border d-grid gap-2">
                            <Button name="dashboard" onClick={() => { navigate('/seller/new-product') }} variant="light" size="lg">Tambah Produk</Button>
                        </div>
                    </li>
                </ul>
                <hr />
                <h4>Profil</h4>
                <hr />
                <ul>
                    <li>
                        <div className="button-border d-grid gap-2">
                            <Button name="profile" onClick={() => { navigate('/seller/profile') }} variant="light" size="lg">Profil Saya</Button>
                        </div>
                    </li>
                    <li>
                        <div className="button-border d-grid gap-2">
                            <Button name="editprofile" onClick={() => { navigate('/seller/change-profile') }} variant="light" size="lg">Edit Profil</Button>
                        </div>
                    </li>
                </ul>
                <hr />
                <h4>Akun</h4>
                <hr />
                <ul>
                    <li>
                        <div className="button-border d-grid gap-2">
                            <Button name="dashboard" onClick={() => {
                                setStore(null)
                                localStorage.clear()
                                navigate('/')
                            }} variant="light" size="lg">Log Out</Button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default SidePanel