import React, { useEffect, useState } from 'react';
import SwiperCore, { Virtual, Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

import './swiper.css';

import { Button, Card, Col, Row } from 'react-bootstrap';

import axios from 'axios';

import motorcycle from "../../image/motorcycle.jpg"
import fashionImage from "../../image/fashion2.jpg"
import mobilImage from "../../image/car.jpg"
import bannerImage from "../../image/banner4.jpg"
import { useNavigate } from 'react-router-dom';

import Formatter from '../../Helper/FormatPrice';

// install Virtual module
SwiperCore.use([Virtual, Navigation]);

const SwiperComponent = () => {
    const [product, setProduct] = useState([])
    const [motor, setMotor] = useState([])
    const [mobil, setMobil] = useState([])
    const [fashion, setFashion] = useState([])

    const [motorID, setMotorID] = useState('')
    const [fashionID, setFashionID] = useState('')
    const [mobilID, setMobilID] = useState('')

    let navigate = useNavigate()

    useEffect(() => {
        const fetchCategory = async () => {
            const product = await axios.get("https://new-api-ecommerce.herokuapp.com/products")
            const result = product.data.data.filter(item => item.post_status === "posted")
            setProduct(result)
            // get category id 
            const category = await axios.get("https://new-api-ecommerce.herokuapp.com/product-category")
            const motorID = category.data.data.filter((val) => {
                return val.category_name === "motor"
            })
            setMotorID(motorID[0].category_name)

            const motor = await axios.get(`https://new-api-ecommerce.herokuapp.com/products/category/${motorID[0].id}`)
            const motorPosted = motor.data.data.filter(item => item.post_status === "posted")
            setMotor(motorPosted)
            const fashionID = category.data.data.filter((val) => {
                return val.category_name === "pakaian"
            })
            setFashionID(fashionID[0].category_name)

            const fashion = await axios.get(`https://new-api-ecommerce.herokuapp.com/products/category/${fashionID[0].id}`)
            const fashionPosted = fashion.data.data.filter(item => item.post_status === "posted")
            setFashion(fashionPosted)

            const mobilID = category.data.data.filter((val) => {
                return val.category_name === "mobil"
            })
            setMobilID(mobilID[0].category_name)

            const mobil = await axios.get(`https://new-api-ecommerce.herokuapp.com/products/category/${mobilID[0].id}`)
            const mobilPosted = mobil.data.data.filter(item => item.post_status === "posted")
            setMobil(mobilPosted)
        }
        fetchCategory()
    }, [])

    return (
        <div className="swiper-container">
            <div className="button-offer">
                <h1>Hot Offer </h1>
                <div className="buttonhotoffer align-self-center">
                    <Button onClick={() => { navigate(`/products`) }} variant="primary" size="sm">
                        Lihat Semua
                    </Button>
                </div>
            </div>
            <Swiper
                slidesPerView={5}
                spaceBetween={30}
                navigation={true}
                breakpoints={{
                    100: {
                        slidesPerView: 1,
                    },
                    700: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    768: {
                        slidesPerView: 3,
                        spaceBetween: 30,
                    },

                    1024: {
                        slidesPerView: 4,
                        spaceBetween: 30,
                    },
                    1500: {
                        slidesPerView: 6,
                        spaceBetween: 30,
                    },
                }}
                modules={[Pagination]}
                className="mySwiper"
            >
                {product.map((value, index) => {
                    const changePriceFormat = Formatter(value.price)
                    return (
                        <SwiperSlide key={value.product_name} virtualIndex={index}>

                            <div className='card-swiper'>
                                <Card onClick={() => { navigate(`/product/${value.id}`) }}>
                                    <Card.Img variant="top" src={value.image_url} />
                                    <Card.Body>
                                        <Card.Title style={{ fontSize: "1rem" }}>{value.product_name}</Card.Title>
                                        <hr />
                                        <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>{changePriceFormat}</Card.Title>
                                        <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>Stok : {value.stock}</Card.Title>
                                    </Card.Body>
                                </Card>
                            </div>

                        </SwiperSlide>
                    )
                })}
            </Swiper>
            <hr />

            {/* banner */}
            <Row>
                <Col sm="12">
                    <div className='banner-container'>
                        <img src={bannerImage} alt="gambar-motor" />
                    </div>
                </Col>
            </Row>
            <hr />
            {/* swiper untuk kategori motor */}
            <div className='motor-swiper-container'>
                <Row>
                    <Col sm="2">
                        <div className='image-motor-swiper-container'>
                            <h1>Motor</h1>
                            <div className="d-block w-15 image-shadow">
                                <img onClick={() => { navigate(`/category/${motorID}`) }} src={motorcycle} alt="gambar-motor" />
                            </div>
                        </div>
                    </Col>
                    <Col sm="10">
                        <div className='motor-swipers'>
                            <Swiper
                                slidesPerView={5}
                                spaceBetween={30}
                                navigation={true}
                                breakpoints={{
                                    100: {
                                        slidesPerView: 1,
                                    },
                                    450: {
                                        slidesPerView: 2,
                                        spaceBetween: 5,

                                    },
                                    768: {
                                        slidesPerView: 3,
                                        spaceBetween: 30,
                                    },

                                    1024: {
                                        slidesPerView: 4,
                                        spaceBetween: 30,
                                    },
                                    1500: {
                                        slidesPerView: 6,
                                        spaceBetween: 30,
                                    },
                                }}
                            >
                                {motor.map((value, index) => {
                                    const changeMotorPriceFormat = Formatter(value.price)
                                    return (
                                        <SwiperSlide key={value.product_name} virtualIndex={index}>
                                            <div className='card-swiper'>
                                                <Card onClick={() => { navigate(`/product/${value.id}`) }}>
                                                    <Card.Img variant="top" src={value.image_url} />
                                                    <Card.Body>
                                                        <Card.Title style={{ fontSize: "1rem" }}>{value.product_name}</Card.Title>
                                                        <hr />
                                                        <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>{changeMotorPriceFormat}</Card.Title>
                                                        <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>Stok : {value.stock}</Card.Title>
                                                    </Card.Body>
                                                </Card>
                                            </div>
                                        </SwiperSlide>
                                    )
                                })}
                            </Swiper>
                        </div>
                    </Col>
                </Row>
            </div>

            <hr />

            {/* Swiper untuk kategori pakaian */}
            <div className='motor-swiper-container'>
                <Row>

                    <Col sm="10">
                        <div className='motor-swipers'>
                            <Swiper
                                slidesPerView={5}
                                spaceBetween={30}
                                navigation={true}
                                breakpoints={{
                                    100: {
                                        slidesPerView: 1,
                                    },
                                    450: {
                                        slidesPerView: 2,
                                    },
                                    768: {
                                        slidesPerView: 3,
                                        spaceBetween: 30,
                                    },

                                    1024: {
                                        slidesPerView: 4,
                                        spaceBetween: 30,
                                    },
                                    1500: {
                                        slidesPerView: 6,
                                        spaceBetween: 30,
                                    },
                                }}
                            >
                                {fashion.map((value, index) => {
                                    const changeFashionPriceFormat = Formatter(value.price)
                                    return (
                                        <SwiperSlide key={value.product_name} virtualIndex={index}>
                                            <div className='card-swiper'>
                                                <Card onClick={() => { navigate(`/product/${value.id}`) }}>
                                                    <Card.Img variant="top" src={value.image_url} />
                                                    <Card.Body>
                                                        <Card.Title style={{ fontSize: "1rem" }}>{value.product_name}</Card.Title>
                                                        <hr />
                                                        <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>{changeFashionPriceFormat}</Card.Title>
                                                        <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>Stok : {value.stock}</Card.Title>
                                                    </Card.Body>
                                                </Card>
                                            </div>
                                        </SwiperSlide>
                                    )
                                })}
                            </Swiper>
                        </div>
                    </Col>
                    <Col sm="2">
                        <div className='image-motor-swiper-container'>
                            <h1>Fashion</h1>
                            <div className="image-shadow">
                                <img onClick={() => { navigate(`/category/${fashionID}`) }} src={fashionImage} alt="gambar-pakaian" />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>

            {/* Swiper untuk kategori Mobil */}
            <div className='motor-swiper-container'>
                <Row>
                    <Col sm="2">
                        <div className='image-motor-swiper-container'>
                            <h1>Mobil</h1>
                            <div className="image-shadow">
                                <img onClick={() => { navigate(`/category/${mobilID}`) }} src={mobilImage} alt="gambar-pakaian" />
                            </div>
                        </div>
                    </Col>
                    <Col sm="10">
                        <div className='motor-swipers'>
                            <Swiper
                                slidesPerView={5}
                                spaceBetween={30}
                                navigation={true}
                                breakpoints={{
                                    100: {
                                        slidesPerView: 1,
                                    },
                                    450: {
                                        slidesPerView: 2,
                                    },
                                    768: {
                                        slidesPerView: 3,
                                        spaceBetween: 30,
                                    },

                                    1024: {
                                        slidesPerView: 4,
                                        spaceBetween: 30,
                                    },
                                    1500: {
                                        slidesPerView: 6,
                                        spaceBetween: 30,
                                    },
                                }}
                            >
                                {mobil.map((value, index) => {
                                    const changeMobilPriceFormat = Formatter(value.price)
                                    return (
                                        <SwiperSlide key={value.product_name} virtualIndex={index}>
                                            <div className='card-swiper'>
                                                <Card onClick={() => { navigate(`/product/${value.id}`) }} >
                                                    <Card.Img variant="top" src={value.image_url} />
                                                    <Card.Body>
                                                        <Card.Title style={{ fontSize: "1rem" }}>{value.product_name}</Card.Title>
                                                        <hr />
                                                        <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>{changeMobilPriceFormat}</Card.Title>
                                                        <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>Stok : {value.stock}</Card.Title>
                                                    </Card.Body>
                                                </Card>
                                            </div>
                                        </SwiperSlide>
                                    )
                                })}
                            </Swiper>
                        </div>
                    </Col>

                </Row>
            </div>
            <br />
        </div >
    );
}

export default SwiperComponent