import './carousel.css';

import { Carousel } from "react-bootstrap";


import banner1 from "../../image/banner-example.jpg"
import banner2 from "../../image/banner-example2.jpg"
import banner3 from "../../image/banner-example3.jpg"

const CarouselComp = () => {

    return (
        <div className="carousel-container-comp">

            <Carousel>
                <Carousel.Item interval={3000}>
                    <img
                        className="d-block w-100"
                        src={banner1}
                        alt="First slide"
                    />
                </Carousel.Item>
                <Carousel.Item interval={3000}>
                    <img
                        className="d-block w-100"
                        src={banner2}
                        alt="Second slide"
                    />
                </Carousel.Item>
                <Carousel.Item interval={3000}>
                    <img
                        className="d-block w-100"
                        src={banner3}

                        alt="Third slide"
                    />
                </Carousel.Item>
            </Carousel>

        </div>

    );
}

export default CarouselComp;
