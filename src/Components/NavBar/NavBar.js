import './NavBar.css';
import { Button, Container, FormControl, InputGroup, Modal, Nav, Navbar, OverlayTrigger, Popover } from 'react-bootstrap';
import { Person, Search } from 'react-bootstrap-icons';
import { useContext, useState } from 'react';

import { StoreContext } from "../../Context/storeContext"
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { AdminContext } from '../../Context/adminContext';


const NavBar = () => {
    const [store,] = useContext(StoreContext)
    const [admin,] = useContext(AdminContext)

    const [input, setInput] = useState('')
    const [, setError] = useState()

    let navigate = useNavigate()

    const handleSubmit = (event) => {
        event.preventDefault()
        axios.get(`https://new-api-ecommerce.herokuapp.com/product/search/:${input}`)
            .then((res) => {
                navigate(`/product/search/${input}`)
            })
            .catch((err) => {
                setError(err.response.data)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        setInput(value)
    }

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);

    return (
        <>  <div className="emptydiv">

        </div>
            <Navbar fixed="top" className="navbar-container" collapseOnSelect expand="lg" >
                <Container>
                    <Navbar.Brand href="/">KulakPedia</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/">Beranda</Nav.Link>
                            <Nav.Link href="/products">Semua Produk</Nav.Link>
                            <div className='searchbar'>
                                <InputGroup >
                                    <FormControl
                                        aria-label="Example text with button addon"
                                        aria-describedby="basic-addon1"
                                        onChange={handleChange}
                                        name="search"
                                    />
                                    <Button onClick={handleSubmit} variant="warning" id="button-addon1">
                                        <Search />
                                    </Button>
                                </InputGroup>
                            </div>
                        </Nav>

                        <Nav className='align-navbar'>


                            <Button onClick={() => {
                                if (store !== null) {
                                    navigate("/seller/new-product")
                                } else {
                                    setShow(true)
                                }
                            }} variant="warning">+ Pasang Iklan</Button>

                            < Modal show={show} onHide={handleClose} >
                                <Modal.Header closeButton>
                                    <Modal.Title>Oopsss,</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>Silahkan Login Terlebih Dahulu</Modal.Body>
                                <Modal.Footer>
                                    <Button variant="warning" onClick={handleClose}>
                                        Close
                                    </Button>
                                </Modal.Footer>
                            </Modal>

                            {store !== null && (<>
                                <Nav.Link href="/seller/dashboard">Dashboard</Nav.Link>
                            </>)}
                            {store == null && admin == null && (<>
                                <Nav.Link href="/seller/login">Masuk</Nav.Link>
                                <Nav.Link href="/seller/register">Daftar</Nav.Link>
                            </>)}
                            {admin !== null && (<>
                                <Nav.Link href="/admin/dashboard">Dashboard</Nav.Link>
                            </>)}
                            <Person className='fa-3x' color="white" />

                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar >


        </>

    );
}

export default NavBar;
