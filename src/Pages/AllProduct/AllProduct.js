import "./allproduct.css"

import iklan1 from "../../image/iklan1.jpg"

import { Card, Col, Row } from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import { useEffect, useState } from "react"
import axios from "axios"

const AllProduct = () => {

    const [data, setData] = useState([])

    useEffect(() => {
        const fetchData = async () => {

            const dataSearch = await axios.get(`https://new-api-ecommerce.herokuapp.com/products`)
            const result = dataSearch.data.data.filter(item => item.post_status === "posted")
            setData(result)

        }
        fetchData()
    }, [])

    let navigate = useNavigate()

    return (
        <div className="category-page-container">
            <Row>
                <div className="category-page-content">
                    <Col sm="10">
                        <div className='iklan-add-container'>
                            <img src={iklan1} alt="iklan1" />
                        </div>
                    </Col>
                    <h1>Semua Produk :</h1>
                    <br />
                    <div className="all-product-container">

                        <div className='card-categorycomp-container' >
                            {data.map((val) => {
                                return (<>
                                    <div className='content-card-categorycomp'>
                                        <Card onClick={() => { navigate(`/product/${val.id}`) }} style={{ width: '10rem' }}>
                                            <div className="shadow-box">
                                                <Card.Img variant="top" src={val.image_url} />
                                                <Card.Body>
                                                    <Card.Title>{val.product_name}</Card.Title>
                                                </Card.Body>
                                            </div>

                                        </Card>
                                    </div>
                                </>)
                            })}
                        </div>
                    </div>

                </div>
            </Row >
        </div >
    )
}

export default AllProduct