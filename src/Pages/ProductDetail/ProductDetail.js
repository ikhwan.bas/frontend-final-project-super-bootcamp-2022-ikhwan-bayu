import "./productdetail.css"

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

import { Card, Col, Row } from "react-bootstrap"

import iklan1 from "../../image/iklan1.jpg"
import profile2 from "../../image/store-profile.jpg"

import { useNavigate, useParams } from "react-router-dom"
import { useEffect, useState } from "react"
import axios from "axios"
import Formatter from "../../Helper/FormatPrice";

const ProductDetail = () => {

    let { id } = useParams()

    const [data, setData] = useState([])
    const [store, setStore] = useState([])
    const [storeprofile, setStoreprofile] = useState([])

    const [category, setCategory] = useState([])

    const [otherdata, setOtherdata] = useState([])

    let navigate = useNavigate()

    useEffect(() => {
        const fetchData = async () => {
            // get product by id 
            const productData = await axios.get(`https://new-api-ecommerce.herokuapp.com/product/${id}`)

            setData(productData.data.data[0])
            setStore(productData.data.data[0].Stores)
            setStoreprofile(productData.data.data[0].Stores.StoresProfile)
            setCategory(productData.data.data[0].ProductCategory)

            const otherData = await axios.get(`https://new-api-ecommerce.herokuapp.com/products/category/${productData.data.data[0].ProductCategoryID}`)
            const result = otherData.data.data.filter(item => item.post_status === "posted")

            setOtherdata(result)

        }
        fetchData()
    }, [id])


    const changeDataPriceFormat = Formatter(data.price)
    return (
        <div className="product-detail-container">
            <Row>
                <Col sm="1"></Col>
                <Col sm="10">
                    <div className="banner-container">
                        <img src={iklan1} alt="iklan1" />
                    </div>
                    <div className="content-all-container">
                        <div className="content-container">
                            <h1>{data.product_name}</h1>
                            <br />

                            <img src={data.image_url} alt="iklan1" />
                            <br />
                            <br />
                            <h4>Deskripsi Produk</h4>
                            <p>{data.description}</p>
                            <br />
                            <h6>Jumlah Stok Tersedia : {data.stock}</h6>
                            <h6>Harga : {changeDataPriceFormat}</h6>
                        </div>
                        <div className="profile-content">
                            <div className="profile-store-container">
                                <img src={profile2} alt="store1" />
                            </div>
                            <h1>Profil Penjual</h1>
                            <h3>{store.store_name}</h3>
                            <p>{storeprofile.city}</p>
                            <p>Alamat Toko : {storeprofile.address}</p>
                            <p>Kode Pos : {storeprofile.postal_code}</p>
                            <p>Nomot Telepon : 0{storeprofile.phone_number}</p>
                        </div>
                    </div>
                    <div className="content-line">

                    </div>

                    <div className="content-related-product">
                        <h2>Produk {category.category_name} Lainnya : </h2>
                        <br />
                        <Swiper
                            slidesPerView={6}
                            spaceBetween={30}
                            navigation={true}
                            breakpoints={{
                                100: {
                                    slidesPerView: 1,
                                },
                                450: {
                                    slidesPerView: 2,
                                },
                                700: {
                                    slidesPerView: 3,
                                    spaceBetween: 10
                                },
                                768: {
                                    slidesPerView: 4,
                                    spaceBetween: 10,
                                },

                                1024: {
                                    slidesPerView: 5,
                                    spaceBetween: 30,
                                },
                                1500: {
                                    slidesPerView: 6,
                                    spaceBetween: 30,
                                },
                            }}
                        >
                            {otherdata.map((value, index) => {
                                const changePriceFormat = Formatter(value.price)
                                return (
                                    <SwiperSlide key={value.product_name} virtualIndex={index}>
                                        <div className='card-swiper'>
                                            <Card onClick={() => { navigate(`/product/${value.id}`) }}>
                                                <Card.Img variant="top" src={value.image_url} />
                                                <Card.Body>
                                                    <Card.Title style={{ fontSize: "1rem" }}>{value.product_name}</Card.Title>
                                                    <hr />
                                                    <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>{changePriceFormat}</Card.Title>
                                                    <Card.Title style={{ fontSize: "0.9rem", textAlign: "left" }}>Stok : {value.stock}</Card.Title>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </SwiperSlide>
                                )
                            })}
                        </Swiper>

                    </div>
                </Col>
                <Col sm="1"></Col>

            </Row>

        </div>
    )
}

export default ProductDetail