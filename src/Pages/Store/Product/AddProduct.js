import "./addProduct.css"
import { Alert, Button, Form } from "react-bootstrap"
import SidePanel from "../../../Components/Sidepanel/Sidepanel"
import { useNavigate } from "react-router-dom"
import { useContext, useEffect, useState } from "react"
import axios from "axios"
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { v4 } from "uuid";

import { StoreContext } from "../../../Context/storeContext"
import { storage } from "../../../Helper/firebase"

const AddProduct = () => {
    const [store,] = useContext(StoreContext)
    const [input, setInput] = useState({ product_name: "", stock: 0, price: 0, weight: 0, description: "", image_url: "", product_category_id: "" })
    const [error, setError] = useState()
    const [category, setCategory] = useState([])
    const [imageUrl, setImageUrl] = useState("")


    let navigate = useNavigate()

    useEffect(() => {
        const fetchCategory = async () => {
            const categoryData = await axios.get("https://new-api-ecommerce.herokuapp.com/product-category")
            setCategory(categoryData.data.data)
        }
        fetchCategory()
    }, [])

    const handleUpload = (event) => {
        const image = event.target.files[0]
        if (image == null) {
            return
        } else {
            const imageRef = ref(storage, `kulakpedia/${image.name + v4()}`);
            // upload image to firebase
            uploadBytes(imageRef, image).then((snapshot) => {
                alert('Image Uploaded')
                getDownloadURL(snapshot.ref).then((downloadURL) => {
                    setImageUrl(downloadURL)
                });
            })
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        axios.post("https://new-api-ecommerce.herokuapp.com/store/products",
            {
                product_name: input.product_name,
                stock: parseInt(input.stock),
                price: parseInt(input.price),
                weight: parseInt(input.weight),
                description: input.description,
                image_url: imageUrl,
                product_category_id: input.product_category_id
            }
            , { headers: { Authorization: "Bearer " + store.token } }
        )
            .then((res) => {
                navigate('/seller/dashboard')
            })
            .catch((err) => {
                setError(err.response.data)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({ ...input, [name]: value })
    }

    return (
        <>
            <div className="add-product-container">
                <div>
                    <SidePanel />
                </div>
                <div className="form-dashboard-container">
                    <h1>Tambah Produk Baru</h1> <br />
                    {error !== undefined && (
                        <Alert severity="error">Please fill the form correctly</Alert>
                    )}
                    <Form>
                        <Form.Group className="mb-4" >
                            <Form.Label>Nama Produk</Form.Label>
                            <Form.Control name="product_name" onChange={handleChange} type="product_name" placeholder="Masukkan Nama Barang" />
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Stok </Form.Label>
                            <Form.Control name="stock" onChange={handleChange} type="stock" placeholder="Masukkan Jumlah Stok Barang" />
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Harga</Form.Label>
                            <Form.Control name="price" onChange={handleChange} type="price" placeholder="Masukkan Harga Barang" />
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Berat</Form.Label>
                            <Form.Control name="weight" onChange={handleChange} type="weight" placeholder="Masukkan Berat Barang (dalam satuan gram)" />
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Deskripsi Barang</Form.Label>
                            <Form.Control name="description" onChange={handleChange} as="textarea" type="description" placeholder="Masukkan Deskripsi Barang" />
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>URL Gambar</Form.Label>
                            <div className="upload">
                                <input type="file" onChange={handleUpload} />
                            </div>
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Kategori Produk</Form.Label>
                            <Form.Select onChange={handleChange} name="product_category_id" aria-label="Default select example">
                                <option>Pilih Kategori Produk</option>
                                {category.map((cat) => {
                                    return (
                                        <>
                                            <option value={cat.id}>{cat.category_name}</option>
                                        </>)
                                })}
                            </Form.Select>
                        </Form.Group>

                        <Button onClick={handleSubmit} className="button-add-product" type="submit">
                            Tambah Produk
                        </Button>
                    </Form>
                </div>
            </div>
        </>
    )
}

export default AddProduct