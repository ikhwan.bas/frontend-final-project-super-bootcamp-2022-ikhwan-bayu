import "./Profile.css"
import { Alert, Button, Form } from "react-bootstrap"
import SidePanel from "../../../Components/Sidepanel/Sidepanel"
import { useNavigate } from "react-router-dom"
import { useContext, useEffect, useState } from "react"
import axios from "axios"

import { StoreContext } from "../../../Context/storeContext"


const EditProfile = () => {
    const [store,] = useContext(StoreContext)
    const [input, setInput] = useState({ address: "", phone_number: 0, postal_code: 0, city: "" })
    const [error, setError] = useState()

    let navigate = useNavigate()

    useEffect(() => {
        const fetchProfile = async () => {
            // Get profile data
            const result = await axios.get("https://new-api-ecommerce.herokuapp.com/store/profile", { headers: { Authorization: "Bearer " + store.token } })
            let {
                address,
                phone_number,
                city,
                postal_code,
            } = result.data.data[0]

            setInput({
                address,
                phone_number,
                city,
                postal_code,
            })
            console.log(result.data.data[0])
        }
        fetchProfile()
    }, [store.token])

    const handleSubmit = (event) => {
        event.preventDefault()

        axios.put("https://new-api-ecommerce.herokuapp.com/store/profile",
            {
                address: input.address,
                city: input.city,
                phone_number: parseInt(input.phone_number),
                postal_code: parseInt(input.postal_code),
            }
            , { headers: { Authorization: "Bearer " + store.token } }
        )
            .then((res) => {
                navigate('/seller/profile')
            })
            .catch((err) => {
                setError(err.response.data)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({ ...input, [name]: value })
    }

    return (
        <>
            <div className="edit-profile-container">
                <div>
                    <SidePanel />
                </div>
                <div className="form-dashboard-container">
                    <h1>Edit Profil Saya</h1> <br />
                    {error !== undefined && (
                        <Alert severity="error">Please fill the form correctly</Alert>
                    )}
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-4" >
                            <Form.Label>Alamat Toko</Form.Label>
                            <Form.Control value={input.address || ''} name="address" onChange={handleChange} type="address" placeholder="Masukkan Alamat Toko" />
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Nomor Telepon </Form.Label>
                            <Form.Control value={input.phone_number || 0} name="phone_number" onChange={handleChange} type="phone_number" placeholder="Masukkan Nomor Telepon" />
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Kota Domisili</Form.Label>
                            <Form.Control value={input.city || ''} name="city" onChange={handleChange} type="city" placeholder="Masukkan Kota Lokasi Toko" />
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Kode Pos</Form.Label>
                            <Form.Control value={input.postal_code || 0} name="postal_code" onChange={handleChange} type="postal_code" placeholder="Masukkan Kode Pos" />
                        </Form.Group>

                        <Button className="button-edit-profile" type="submit">
                            Edit Profile
                        </Button>
                    </Form>
                </div>
            </div>
        </>
    )
}

export default EditProfile