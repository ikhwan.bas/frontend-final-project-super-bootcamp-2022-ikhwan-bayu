import "./Profile.css"
import SidePanel from "../../../Components/Sidepanel/Sidepanel"
import { useContext, useEffect, useState } from "react"
import axios from "axios"

import { StoreContext } from "../../../Context/storeContext"
import { Col, Form, Row } from "react-bootstrap"

const Profile = () => {
    const [store,] = useContext(StoreContext)
    const [data, setData] = useState({ email: "", store_name: "", address: "", city: "", phone_number: 0, postal_code: 0 })

    useEffect(() => {
        const fetchProfile = async () => {
            // Get profile data
            const storeData = await axios.get("https://new-api-ecommerce.herokuapp.com/store", { headers: { Authorization: "Bearer " + store.token } })
            let { email, store_name } = storeData.data.data[0]

            const profile = await axios.get("https://new-api-ecommerce.herokuapp.com/store/profile", { headers: { Authorization: "Bearer " + store.token } })
            let { address, city, phone_number, postal_code } = profile.data.data[0]

            setData({
                email,
                store_name,
                address,
                city,
                phone_number,
                postal_code

            })
        }
        fetchProfile()
    }, [store.token, setData])

    return (
        <>
            <div className="edit-profile-container">
                <div>
                    <SidePanel />
                </div>
                <div className="profile-container">
                    <h1>Hi , {data.store_name}</h1> <br />
                    <div className="content-profile">

                        <Form>
                            <Form.Group className="mb-3" controlId="formPlaintextEmail">
                                <Row>
                                    <Form.Label column sm="3">
                                        Nama Toko
                                    </Form.Label>
                                    <Col sm="9">
                                        <Form.Control placeholder={data.store_name} disabled />
                                    </Col>
                                </Row>
                                <br />
                                <Row>
                                    <Form.Label column sm="3">
                                        Email
                                    </Form.Label>
                                    <Col sm="9">
                                        <Form.Control placeholder={data.email} disabled />
                                    </Col>
                                </Row>

                                <br />
                                <Row>
                                    <Form.Label column sm="3">
                                        Alamat
                                    </Form.Label>
                                    <Col sm="9">
                                        <Form.Control placeholder={data.address} disabled />
                                    </Col>
                                </Row>

                                <br />
                                <Row>
                                    <Form.Label column sm="3">
                                        Kota
                                    </Form.Label>
                                    <Col sm="9">
                                        <Form.Control placeholder={data.city} disabled />
                                    </Col>
                                </Row>

                                <br />
                                <Row>
                                    <Form.Label column sm="3">
                                        Kode Pos
                                    </Form.Label>
                                    <Col sm="9">
                                        <Form.Control placeholder={data.postal_code} disabled />
                                    </Col>
                                </Row>

                                <br />
                                <Row>
                                    <Form.Label column sm="3">
                                        Nomor Telepon
                                    </Form.Label>
                                    <Col sm="9">
                                        <Form.Control placeholder={data.phone_number} disabled />
                                    </Col>
                                </Row>
                            </Form.Group>
                        </Form>


                    </div>
                </div>

            </div>
        </>
    )
}

export default Profile