import axios from "axios";
import { useState } from "react";
import { Alert, Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import vector1 from "../../../image/sizemini_97.jpg"



const Register = () => {
    const [input, setInput] = useState({ store_name: "", email: "", password: "" })
    const [error, setError] = useState()

    let navigate = useNavigate()

    const handleSubmit = (event) => {
        event.preventDefault()
        axios.post("https://new-api-ecommerce.herokuapp.com/store/register", {
            store_name: input.store_name,
            email: input.email,
            password: input.password
        })
            .then((res) => {
                navigate('/seller/login')
            })
            .catch((err) => {
                setError(err.response.data)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({ ...input, [name]: value })
    }

    return (
        <>
            <div className="container">
                <div className="container register-container">

                    <div className="image-container">
                        <img
                            className="d-block w-100"
                            src={vector1}

                            alt="First slide"
                        />
                    </div>

                    <div className="container form-container">
                        <Form onSubmit={handleSubmit}>
                            <h1>Daftar Akun</h1>
                            <br></br>
                            {error !== undefined && (
                                <Alert key="danger" variant="danger">Please fill this form correctly</Alert>
                            )}

                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Nama Toko</Form.Label>
                                <Form.Control name="store_name" onChange={handleChange} type="storename" placeholder="Masukkan Nama Toko" />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control name="email" onChange={handleChange} type="email" placeholder="Masukkan Email" />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control name="password" onChange={handleChange} type="password" placeholder="Masukkan Password" />
                            </Form.Group>
                            <br />
                            <Button variant="primary" type="submit">
                                Daftar
                            </Button>
                            <br />
                            <br />
                            <Form.Text className="text-muted">
                                Sudah punya akun KulakPedia ? <a href='/seller/login'>Masuk Sekarang</a>
                            </Form.Text>
                        </Form>
                    </div>
                </div>
            </div>
            <br />
            <br />

        </>

    );
}

export default Register;
