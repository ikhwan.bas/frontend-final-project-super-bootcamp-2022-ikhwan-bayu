import './dashboard.css';

import SidePanel from '../../../Components/Sidepanel/Sidepanel';
import { Nav, Col, Row, Table } from 'react-bootstrap';
import { useContext, useEffect, useState } from 'react';
import { StoreContext } from '../../../Context/storeContext';
import axios from 'axios';
import Formatter from '../../../Helper/FormatPrice';
import _ from 'lodash'

const Dashboard = () => {
    const [store,] = useContext(StoreContext)
    const [product, setProduct] = useState([])

    const [paginatedproduct, setPaginatedproduct] = useState([])
    const [currentPage, setCurrentPage] = useState(1)

    // pagination
    const pageSize = 5
    const pageCount = Math.ceil(product.length / pageSize)
    const pages = _.range(1, pageCount + 1)


    useEffect(() => {
        const fetchCategory = async () => {
            const products = await axios.get("https://new-api-ecommerce.herokuapp.com/store/products", { headers: { Authorization: "Bearer " + store.token } })
            setProduct(products.data.data)
            setPaginatedproduct(_(products.data.data).slice(0).take(pageSize).value())
        }
        fetchCategory()
    }, [store.token])

    const pagination = (pageNo) => {
        setCurrentPage(pageNo)
        const startIndex = (pageNo - 1) * pageSize
        const paginatedproducts = _(product).slice(startIndex).take(pageSize).value()
        setPaginatedproduct(paginatedproducts)
    }


    return (
        <>
            <div className='dashboard-container'>
                <div >
                    <SidePanel />
                </div>
                <div className="dashboard-table-container">
                    <Row>
                        <Col sm="1"></Col>
                        <Col sm="10">
                            <h1>Produk Saya</h1>
                            <br />
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Produk</th>
                                        <th>deskripsi</th>
                                        <th>Stok</th>
                                        <th>Harga</th>
                                        <th>Berat (gram)</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {paginatedproduct.map((val, ind) => {
                                        const changeFormat = Formatter(val.price)
                                        return (
                                            <>
                                                <tr>
                                                    <td>{(currentPage - 1) * 5 + (ind + 1)}</td>
                                                    <td>{val.product_name}</td>
                                                    <td>{val.description}</td>
                                                    <td>{val.stock}</td>
                                                    <td>{changeFormat}</td>
                                                    <td>{val.weight}</td>
                                                    <td>{val.post_status}</td>
                                                </tr>
                                            </>
                                        )
                                    })}

                                </tbody>
                            </Table>
                            <Nav className="d-flex justify-content-center">
                                <ul className="pagination">
                                    {pages.map((page) => {
                                        return (
                                            <li className={page === currentPage ? "page-item active" : "page-item"}>
                                                <p className='page-link' onClick={() => pagination(page)}>
                                                    {page}
                                                </p>
                                            </li>)
                                    })}
                                </ul>
                            </Nav>
                        </Col>
                        <Col sm="1"></Col>

                    </Row>

                </div>
            </div>
        </>
    )
}

export default Dashboard