import './homepage.css';
import "swiper/css/bundle";

import { Col, Row } from 'react-bootstrap';
import CarouselComp from "../../Components/Carousel/Carousel";
import Category from '../../Components/Category/Category';
import SwiperComponent from '../../Components/Swiper/SwiperHotOFfer';

import iklan1 from "../../image/iklan1.jpg"


const Home = () => {


    return (
        <div className='homepage-container' >
            <div className='carousel-content'>
                <Row>
                    <Col sm="1"></Col>
                    <Col sm="10">
                        <CarouselComp />
                    </Col>
                    <Col sm="1"></Col>
                </Row>
            </div>

            <div className='category-container'>
                <Row>
                    <Col sm="1"></Col>
                    <Col sm="10">
                        <h2>Kategori Pilihan</h2><br />
                        <div className='container card-category-container-box'>
                            <Category />
                        </div>


                    </Col>
                    <Col sm="1"></Col>
                </Row>

            </div>
            <div className='hot-offer-container'>
                <Row>
                    <Col sm="1"></Col>
                    <Col sm="10">
                        <hr />
                        <div className='hot-offer-container-content'>
                            <SwiperComponent />
                        </div>


                    </Col>
                    <Col sm="1"></Col>
                </Row>

            </div>
            <br />

            <div className='image-add-container'>
                <Row>
                    <Col sm="1"></Col>
                    <Col sm="10">
                        <div className='image-add-container'>
                            <img src={iklan1} alt="iklan1" />
                        </div>
                    </Col>
                    <Col sm="1"></Col>
                </Row>
            </div>
            <br />
        </div>
    );
}

export default Home;
