import { useContext, useEffect, useState } from "react";
import { AdminContext } from "../../../Context/adminContext";
import axios from "axios";
import { Button, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Formatter from "../../../Helper/FormatPrice";

const PostedProduct = () => {

    const [admin, setAdmin] = useContext(AdminContext);
    const [product, setProduct] = useState([])
    const [, setError] = useState()

    const [fetchTrigger, setFetchTrigger] = useState(true);

    let navigate = useNavigate()
    useEffect(() => {
        const fetchData = async () => {
            const product = await axios.get("https://new-api-ecommerce.herokuapp.com/products")
            const result = product.data.data.filter(item => item.post_status === "posted")
            setProduct(result)
        }
        if (fetchTrigger) {
            fetchData();
            setFetchTrigger(false);
        }
    }, [fetchTrigger])

    const handleDelete = async (event) => {
        event.preventDefault()
        await axios.put(`https://new-api-ecommerce.herokuapp.com/admin/product/unpost/${event.target.id}`, {}, { headers: { Authorization: "Bearer " + admin.token } }
        )
            .then((res) => {
                setFetchTrigger(true)
                navigate('/admin/dashboard')
            })
            .catch((err) => {
                setError(err.response.data)
            })
    }

    return (
        admin !== null && (
            <div className="container-dashboard-admin">
                <div className="sidebar-admin-container">
                    <h3>Admin</h3>
                    <hr />
                    <ul>
                        <h4>Produk</h4>
                        <div className="sidebar-button">
                            <li>
                                <a href="/admin/dashboard">Semua Produk</a>
                            </li>
                        </div>
                        <div className="sidebar-button">
                            <li>
                                <a href="/admin/products/posted">Produk Posted</a>
                            </li>
                        </div>
                        <div className="sidebar-button">
                            <li>
                                <a href="/admin/products/pending">Produk Pending</a>
                            </li>
                        </div>
                        <div className="sidebar-button">
                            <li>
                                <a href="/admin/products/deleted">Produk Deleted</a>
                            </li>
                        </div>
                        <div onClick={() => {
                            setAdmin(null)
                            localStorage.clear()
                            navigate('/')
                        }} className="sidebar-button">
                            <h3>Log Out</h3>
                        </div>
                    </ul>
                </div>

                <div className="container-admin-dashboard">
                    <h1>Produk Posted</h1>
                    <br />
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Produk</th>
                                <th>deskripsi</th>
                                <th>Stok</th>
                                <th>Harga</th>
                                <th>Berat (gram)</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            {product.map((val, ind) => {
                                return (
                                    <tr>
                                        <td>{ind + 1}</td>
                                        <td>{val.product_name}</td>
                                        <td>{val.description}</td>
                                        <td>{val.stock}</td>
                                        <td>{Formatter(val.price)}</td>
                                        <td>{val.weight}</td>
                                        <td>{val.post_status}</td>
                                        <td>
                                            <Button onClick={handleDelete} name="delete" id={val.id} variant="danger" size="sm">
                                                Delete
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })}

                        </tbody>
                    </Table>
                </div>
            </div>
        )
    )
}

export default PostedProduct;