import '../../../Style/authStyle.css';

import axios from "axios";
import vector1 from "../../../image/sizemini_97.jpg"

import { useContext, useState } from "react";
import { Alert, Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { AdminContext } from '../../../Context/adminContext';

const LoginAdmin = () => {

    const [, setAdmin] = useContext(AdminContext)
    const [input, setInput] = useState({ email: "", password: "" })
    const [error, setError] = useState()

    let navigate = useNavigate()

    const handleSubmit = (event) => {
        event.preventDefault();
        axios
            .post("https://new-api-ecommerce.herokuapp.com/admin/login", {
                email: input.email,
                password: input.password,
            })
            .then((res) => {
                var token = res.data.token;
                var currentAdmin = { token };
                setAdmin(currentAdmin);
                localStorage.clear()
                localStorage.setItem("admin", JSON.stringify(currentAdmin));
                navigate("/admin/dashboard");
            })
            .catch((err) => {
                setError(err.response.data);
            });
    };

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({ ...input, [name]: value })
    }

    return (

        <>
            <div className="container">
                <div className="container register-container">

                    <div className="image-container">
                        <img
                            className="d-block w-100"
                            src={vector1}

                            alt="First slide"
                        />
                    </div>

                    <div className="container form-container">
                        <Form onSubmit={handleSubmit}>
                            <h1>Login Admin</h1>
                            <br></br>
                            {error !== undefined && (
                                <Alert key="danger" variant="danger">Please fill this form correctly</Alert>
                            )}

                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control name="email" onChange={handleChange} type="email" placeholder="Masukkan Email" />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control name="password" onChange={handleChange} type="password" placeholder="Masukkan Password" />
                            </Form.Group>
                            <br />
                            <Button variant="primary" type="submit">
                                Login
                            </Button>
                            <br />
                            <br />
                            <Form.Text className="text-muted">
                                Belum memiliki akun ? <a href='/admin/register'>Daftar Sekarang</a>
                            </Form.Text>
                        </Form>
                    </div>
                </div>
            </div>
            <br />
            <br />

        </>



    );
}

export default LoginAdmin;
