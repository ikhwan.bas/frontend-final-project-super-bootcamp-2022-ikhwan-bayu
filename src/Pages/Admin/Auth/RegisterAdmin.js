import axios from "axios";
import { useState } from "react";
import { Alert, Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import vector1 from "../../../image/sizemini_97.jpg"



const RegisterAdmin = () => {
    const [input, setInput] = useState({ verification: "", fullname: "", email: "", password: "" })
    const [error, setError] = useState()

    let navigate = useNavigate()

    const handleSubmit = (event) => {
        event.preventDefault()
        if (input.verification === "1234verification4321") {
            axios.post("https://new-api-ecommerce.herokuapp.com/admin/register", {
                fullname: input.fullname,
                email: input.email,
                password: input.password
            })
                .then((res) => {
                    navigate('/admin/login')
                })
                .catch((err) => {
                    setError(err.response.data)
                })
        } else {
            setError("Verification code is incorrect")
        }

    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({ ...input, [name]: value })
    }

    return (
        <>
            <div className="container">
                <div className="container register-container">

                    <div className="image-container">
                        <img
                            className="d-block w-100"
                            src={vector1}

                            alt="First slide"
                        />
                    </div>

                    <div className="container form-container">
                        <Form onSubmit={handleSubmit}>
                            <h1>Register Admin</h1>
                            <br></br>
                            {error !== undefined && (
                                <Alert key="danger" variant="danger">Please use verified password from the company</Alert>
                            )}

                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Nama Lengkap</Form.Label>
                                <Form.Control name="fullname" onChange={handleChange} type="fullname" placeholder="Masukkan Nama Lengkap" />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control name="email" onChange={handleChange} type="email" placeholder="Masukkan Email" />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control name="password" onChange={handleChange} type="password" placeholder="Masukkan Password" />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Verifikasi Perusahaan</Form.Label>
                                <Form.Control name="verification" onChange={handleChange} type="password" placeholder="Masukkan Password Verifikasi" />
                            </Form.Group>

                            <br />
                            <Button variant="primary" type="submit">
                                Daftar
                            </Button>
                            <br />
                            <br />
                            <Form.Text className="text-muted">
                                Sudah punya akun admin KulakPedia ? <a href='/admin/login'>Masuk Sekarang</a>
                            </Form.Text>
                        </Form>
                    </div>
                </div>
            </div>
            <br />
            <br />

        </>

    );
}

export default RegisterAdmin;
