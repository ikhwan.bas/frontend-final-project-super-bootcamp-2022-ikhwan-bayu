import "./dashboardadmin.css"
import { useContext, useEffect, useState } from "react";
import { AdminContext } from "../../../Context/adminContext";
import axios from "axios";
import { Button, Nav, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import _ from 'lodash'
import Formatter from "../../../Helper/FormatPrice";

const DashboardAdmin = () => {

    const [admin, setAdmin] = useContext(AdminContext);
    const [product, setProduct] = useState([])
    const [, setError] = useState()

    const [fetchTrigger, setFetchTrigger] = useState(true);

    // pagination
    const [paginatedproduct, setPaginatedproduct] = useState([])
    const [currentPage, setCurrentPage] = useState(1)

    const pageSize = 5
    const pageCount = Math.ceil(product.length / pageSize)
    const pages = _.range(1, pageCount + 1)


    let navigate = useNavigate()
    useEffect(() => {
        const fetchData = async () => {
            const products = await axios.get("https://new-api-ecommerce.herokuapp.com/products")
            setProduct(products.data.data)
            setPaginatedproduct(_(products.data.data).slice(0).take(pageSize).value())

        }
        if (fetchTrigger) {
            fetchData();
            setFetchTrigger(false);
        }
    }, [fetchTrigger])

    const handlePost = async (event) => {
        event.preventDefault()
        await axios.put(`https://new-api-ecommerce.herokuapp.com/admin/product/post/${event.target.id}`, {}, { headers: { Authorization: "Bearer " + admin.token } }
        )
            .then((res) => {
                setFetchTrigger(true)
                navigate('/admin/dashboard')
            })
            .catch((err) => {
                setError(err.response.data)
            })
    }

    const handleDelete = async (event) => {
        event.preventDefault()
        await axios.put(`https://new-api-ecommerce.herokuapp.com/admin/product/unpost/${event.target.id}`, {}, { headers: { Authorization: "Bearer " + admin.token } }
        )
            .then((res) => {
                setFetchTrigger(true)
                navigate('/admin/dashboard')
            })
            .catch((err) => {
                setError(err.response.data)
            })
    }

    const pagination = (pageNo) => {
        setCurrentPage(pageNo)
        const startIndex = (pageNo - 1) * pageSize
        const paginatedproducts = _(product).slice(startIndex).take(pageSize).value()
        setPaginatedproduct(paginatedproducts)
    }

    return (
        admin !== null && (
            <div className="container-dashboard-admin">
                <div className="sidebar-admin-container">
                    <h3>Admin</h3>
                    <hr />
                    <ul>
                        <h4>Produk</h4>
                        <div className="sidebar-button">
                            <li>
                                <a href="/admin/dashboard">Semua Produk</a>
                            </li>
                        </div>
                        <div className="sidebar-button">
                            <li>
                                <a href="/admin/products/posted">Produk Posted</a>
                            </li>
                        </div>
                        <div className="sidebar-button">
                            <li>
                                <a href="/admin/products/pending">Produk Pending</a>
                            </li>
                        </div>
                        <div className="sidebar-button">
                            <li>
                                <a href="/admin/products/deleted">Produk Deleted</a>
                            </li>
                        </div>
                    </ul>
                    <div onClick={() => {
                        setAdmin(null)
                        localStorage.clear()
                        navigate('/')
                    }} className="sidebar-button logout-button">
                        <h4>Log Out</h4>
                    </div>
                </div>

                <div className="container-admin-dashboard">
                    <h1>Semua Produk</h1>
                    <br />
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Produk</th>
                                <th>deskripsi</th>
                                <th>Stok</th>
                                <th>Harga</th>
                                <th>Berat (gram)</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            {paginatedproduct.map((val, ind) => {
                                return (
                                    <tr>
                                        <td>{(currentPage - 1) * 5 + (ind + 1)}</td>
                                        <td>{val.product_name}</td>
                                        <td>{val.description}</td>
                                        <td>{val.stock}</td>
                                        <td>{Formatter(val.price)}</td>
                                        <td>{val.weight}</td>
                                        <td>{val.post_status}</td>
                                        <td>
                                            <Button onClick={handlePost} name="post" id={val.id} value={val} variant="success" size="sm">
                                                Post
                                            </Button>
                                            <Button onClick={handleDelete} name="delete" id={val.id} variant="danger" size="sm">
                                                Delete
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })}

                        </tbody>
                    </Table>
                    <Nav className="d-flex justify-content-center">
                        <ul className="pagination">
                            {pages.map((page) => {
                                return (
                                    <li className={page === currentPage ? "page-item active" : "page-item"}>
                                        <p className='page-link' onClick={() => pagination(page)}>
                                            {page}
                                        </p>
                                    </li>)
                            })}
                        </ul>
                    </Nav>
                </div>
            </div>
        )
    )
}

export default DashboardAdmin;