import "./category.css"

import iklan1 from "../../image/iklan1.jpg"

import { Card, Col, Row } from "react-bootstrap"
import { useNavigate, useParams } from "react-router-dom"
import { useEffect, useState } from "react"
import axios from "axios"
import FormatTitle from "../../Helper/FormatTitle"

const CategoryPage = () => {

    let { names } = useParams()

    const [data, setData] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            // get category id 
            const allData = await axios.get("https://new-api-ecommerce.herokuapp.com/product-category")

            const categoryID = allData.data.data.filter((val) => {
                return val.category_name === names
            })

            const dataSearch = await axios.get(`https://new-api-ecommerce.herokuapp.com/products/category/${categoryID[0].id}`)
            const result = dataSearch.data.data.filter(item => item.post_status === "posted")

            setData(result)

        }
        fetchData()
    }, [names])

    let navigate = useNavigate()

    return (
        <div className="category-page-container">
            <Row>
                <Col sm='1'></Col>
                <Col sm='11'>
                    <div className="category-page-content">
                        <Col sm="10">
                            <div className='iklan-add-container'>
                                <img src={iklan1} alt="iklan1" />
                            </div>
                        </Col>
                        <Col sm='10'>
                            <h1>Hasil Pencarian : {names}</h1>
                            <br />
                            <div className='card-categorycomp-container' >
                                {data.map((val) => {
                                    return (<>

                                        <div className='content-card-category'>
                                            <Card onClick={() => { navigate(`/product/${val.id}`) }} style={{ width: '10rem' }}>
                                                <div className="shadow-box">
                                                    <Card.Img variant="top" src={val.image_url} />
                                                    <Card.Body>
                                                        <Card.Title>{val.product_name}</Card.Title>
                                                    </Card.Body>
                                                </div>
                                            </Card>
                                        </div>

                                    </>)
                                })}
                            </div>
                        </Col>
                    </div>
                </Col >
                <Col sm='1'></Col>
            </Row >
        </div >
    )
}

export default CategoryPage