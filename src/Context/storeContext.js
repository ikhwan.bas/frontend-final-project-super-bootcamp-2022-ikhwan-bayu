import { useState, createContext } from "react";

export const StoreContext = createContext();

export const StoreProvider = (props) => {
	// for check current store when browser was refreshed
	const currentStore = JSON.parse(localStorage.getItem("store"));

	// state for user
	const [store, setStore] = useState(currentStore);

	return (
		<StoreContext.Provider value={[store, setStore]}>
			{props.children}
		</StoreContext.Provider>
	);
};
