import 'bootstrap/dist/css/bootstrap.min.css';

import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Home from './Pages/Homepage/Homepage';
import Login from './Pages/Store/Login/Login';
import Register from './Pages//Store/Register/Register';
import NavBar from './Components/NavBar/NavBar';
import Dashboard from './Pages/Store/Dashboard/Dashboard';
import AddProduct from './Pages/Store/Product/AddProduct';
import EditProfile from './Pages/Store/Profile/EditProfile';
import Profile from './Pages/Store/Profile/Profile';
import CategoryPage from './Pages/Category/Category';
import ProductDetail from './Pages/ProductDetail/ProductDetail';
import FooterComp from './Components/Footer/Footer';
import AllProduct from './Pages/AllProduct/AllProduct';
import Search from './Pages/Search/Search';
import LoginAdmin from './Pages/Admin/Auth/LoginAdmin';
import RegisterAdmin from './Pages/Admin/Auth/RegisterAdmin';
import DashboardAdmin from './Pages/Admin/DashboardAdmin/DashboardAdmin';
import PostedProduct from './Pages/Admin/PostedProducts/PostedProduct';
import PendingProduct from './Pages/Admin/PostedProducts/PendingProduct';
import DeletedProduct from './Pages/Admin/PostedProducts/DeletedProduct';

const App = () => {

  return (

    <>
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} />

          <Route path="/category/:names" element={<CategoryPage />} />

          <Route path="/products" element={<AllProduct />} />
          <Route path="/product/:id" element={<ProductDetail />} />
          <Route path="/product/search/:name" element={<Search />} />

          <Route path="/seller/login" element={<Login />} />
          <Route path="/seller/register" element={<Register />} />

          <Route path="/seller/dashboard" element={<Dashboard />} />
          <Route path="/seller/new-product" element={<AddProduct />} />

          <Route path="/seller/profile" element={<Profile />} />
          <Route path="/seller/change-profile" element={<EditProfile />} />

          <Route path="/admin/login" element={<LoginAdmin />} />
          <Route path="/admin/register" element={<RegisterAdmin />} />
          <Route path="/admin/dashboard" element={<DashboardAdmin />} />
          <Route path="/admin/products/posted" element={<PostedProduct />} />
          <Route path="/admin/products/pending" element={<PendingProduct />} />
          <Route path="/admin/products/deleted" element={<DeletedProduct />} />

        </Routes>
        <FooterComp />
      </BrowserRouter>
    </>

  );
}

export default App;
