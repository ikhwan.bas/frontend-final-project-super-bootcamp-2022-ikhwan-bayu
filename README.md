<a href="#"><img width="100%" height="auto" src="https://i.imgur.com/iXuL1HG.png" height="175px"/></a>

<h1 align="center">Hi <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">, Welcome to KulakPedia E-Commerce Application</h1>

## Frontend - Final Project Super Bootcamp - Sanbercode - 2022

## 💫 Description
This is an e-commerce application that was created in Final Project Super Bootcamp Sanbercode 2022.

## 🌱 Installation 
#### To start this project, run
```
npm start or yarn start
```

## Notes :
#### Admin Features access /admin/register & /admin/login

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## 🚀 Languages & Tools:
<p align="left"> 
    <a href="https://git-scm.com/" target="_blank"> <img src="https://img.icons8.com/color/48/000000/git.png" width="40" height="40"/> </a> 
    <a href="https://react-bootstrap.github.io/" target="_blank"> <img src="https://bootstrapcdn.herokuapp.com/assets/img/integrations/react-bootstrap.17ba56f.png" alt="react-bootstrap" width="40" height="40"/> </a>
      <a href="https://reactjs.org/" target="_blank"> <img src="https://logos-download.com/wp-content/uploads/2016/09/React_logo_wordmark.png" alt="react" width="40" height="40"/> </a>
       <a href="https://swiperjs.com/" target="_blank"> <img src="https://miro.medium.com/max/1200/1*Bx6Oi0dqaYnBgnA1syqj9Q.png" alt="swiper" width="40" height="40"/> </a>
       <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"> <img src="https://img.icons8.com/color/48/000000/javascript.png"/> </a> 
</p>

## 🚀 Other Dependencies :
- Swiper JS

## 🚀 Roadmap
Target Development Roadmap : Creating user routes so, people can order a product in our application. 

## ⚡ License
open source projects

## 👨‍ Connect with me:
<p align="left">

<a href = "https://www.linkedin.com/in/ikhwan-bayu-adhi-setiawan/"><img src="https://img.icons8.com/fluent/48/000000/linkedin.png"/></a>
<a href = "https://twitter.com/Ikhwan_IBAS"><img src="https://img.icons8.com/fluent/48/000000/twitter.png"/></a>
<a href = "https://www.instagram.com/ikhwanbas/"><img src="https://img.icons8.com/fluent/48/000000/instagram-new.png"/></a>
<a href = "https://www.youtube.com/channel/UCpwIHV2ECKm7dmNu83muqWQ"><img src="https://img.icons8.com/color/48/000000/youtube-play.png"/></a>

</p>